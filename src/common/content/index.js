// @flow
import showdown from 'showdown';

const content = {
  name: 'David Sinclair',
  feed: [
    {
      type: 'thought',
      slug: 'aws-lambda-api-scaling',
      title: 'AWS Lambda API Scaling',
      description: 'Finding the limits using Gatling against AWS API Gateway & Lambda.',
      date: '2017-02-21',
    },
    {
      type: 'thought',
      slug: 'what-is-fela',
      title: 'What is this Fela?',
      description: 'Checking out one of the newer CSS-in-JavaScript kids on the block.',
      date: '2017-02-07',
    },
    {
      type: 'thought',
      slug: 'react-native-familiarization',
      title: 'React Native Familiarization',
      description: 'Looking to combine web apps with native apps? This is your jam.',
      date: '2017-01-28',
    },
    {
      type: 'thought',
      slug: 'a-first-look-at-claudia-js',
      title: 'A First Look at Claudia.JS',
      description: 'If you have used Serverless, but curious what else is out there.',
      date: '2017-01-14',
    },
    {
      type: 'thought',
      slug: 'serverless-framework-alternatives',
      title: 'Serverless Framework Alternatives',
      description: 'If you have used Serverless, but curious what else is out there.',
      date: '2017-01-01',
    },
    {
      type: 'thought',
      slug: 'the-learning-process',
      title: 'The Learning Process',
      description: 'A high-level dissection of when to use a starter-pack or doing it from scratch.',
      date: '2016-08-01',
    },
    {
      type: 'project',
      slug: 'pathway-genomics',
      title: 'Pathway Genomics',
      date: '2016-07-07',
      images: 5,
    },
    {
      type: 'thought',
      slug: 'some-development-notes',
      title: 'Some Development Notes',
      description: 'A compilation of notes and thoughts after a year of becoming more closely aligned with the world of JavaScript development.',
      date: '2016-04-04',
    },
    {
      type: 'project',
      slug: 'elevated',
      title: 'Elevated',
      external: 'http://elevated.com',
      date: '2015-01-22',
      images: 3,
    },
    {
      type: 'thought',
      slug: 'a-new-cell-phone',
      title: 'A New Cell Phone',
      description: 'Documenting a transition, including pains and gains, from a paying phone service to a free, via exciting apps and the help of the giant, Google.',
      date: '2013-04-12',
    },
    {
      type: 'thought',
      slug: 'what-we-need',
      title: 'What We Need',
      description: 'A comment on the world of options we live in and the direction I hope our behaviors will move toward.',
      date: '2012-08-19',
    },
    {
      type: 'project',
      slug: 'art-tiles-by-carla',
      title: 'Art Tiles by Carla',
      external: 'http://arttilesbycarla.com',
      date: '2012-01-21',
      images: 1,
    },
    {
      type: 'project',
      slug: 'travel-to-go',
      title: 'Travel To Go',
      date: '2012-01-21',
      images: 1,
    },
    {
      type: 'project',
      slug: 'words-for-sense',
      title: 'Words for Sense',
      date: '2012-01-21',
      images: 1,
    },
    {
      type: 'project',
      slug: 'man-moves',
      title: 'Man Moves',
      date: '2012-01-21',
      images: 1,
    },
    {
      type: 'thought',
      slug: 'simplicity-and-complexity',
      title: 'Simplicity & Complexity',
      description: 'A partial review of a book that can shape your perspective on design, life, and everything in between.',
      date: '2011-06-07',
    },
    {
      type: 'project',
      slug: 'dd-studio',
      title: 'DD Studio',
      date: '2011-01-21',
      images: 2,
    },
  ],
  pages: {
    fallback: {
      title: 'Erorr!',
    },
    resume: {
      title: 'Resume',
    },
    about: {
      title: 'David Sinclair',
    },
  },
};

// const converter = new showdown.Converter();
//
// const addContent = (path, item) => {
//   if (process.browser) {
//     // eslint-disable-next-line import/no-dynamic-require
//     item.content = converter.makeHtml(require(path));
//   }
// };
//
// content.feed.forEach(item => {
//   addContent(`./${item.type}s/${item.slug}.md`, item);
// });
//
// Object.keys(content.pages).forEach(key => {
//   addContent(`./pages/${key}.md`, content.pages[key]);
// });

if (process.browser) {
  const context = require.context('./', true, /\.(md)$/);

  context.keys().forEach(filename => {
    addContent(`./pages/${key}.md`, content.pages[key]);
    files[filename] = context(filename);
  });
}

console.log(content);

export default content;
