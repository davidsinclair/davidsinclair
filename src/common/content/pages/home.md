# David Sinclair. I am a developer from San Diego, California. My resume can be seen [here]("/resume") and downloaded [here]("/downloads/David-Sinclair-Resume.pdf").

## What I'm Up To
I just started a new position as a JavaScript engineer at Sony Electronics—it's very hush-hush, so that's all I can. Oh, well I can say that the people there are great.

Before that I was working for Pathway Genomics as a Front End Engineer. My role was to build out health app prototypes and create and maintain web applications used by customers and customer service representatives. I have recent experience coding in React, Backbone, and Angular JavaScript libraries.

I do a decent job of keeping up with the latest JavaScript web crazes, even though it's a little crazy to even try. I am comfortable with all flavors of CSS, including CSS within JavaScript, but long live Stylus! I also have some experience coding PHP API endpoints and some light exposure to Postgres, MySQL, and Mongo databases.

## Project Approach
In taking on a new project, it is helpful to have a code or reference to fall back upon. For myself, this is the Ten Principles of Design by Dieter Rams ([vitsoe.com]("https://www.vitsoe.com/us/about/good-design")). While Rams' principles are not specifically geared towards development, the root concept of a complete, elegant solution still applies. Whether developing a user interface, an API, or anything else, it is important to establish purpose and context to understand how the project will fit into the world around it. In regards to execution, I seek established standards that others can understand. I take pride in my work while always looking to improve my practice.

## Some Career Background
I began creating computer-game websites for fun. While in a Political Science undergraduate program and studying for law school, a variety of website design projects seemed to keep attracting my attention. It became clear my future was in design and development, not law. After finishing my bachelor's degree and continuing my education in Graphic Arts, I am now happily involved in the web community, a field I am passionate about.

## My Root Cause
An important part of my life is my love for the environment. I am doing what I can to live in a way that promotes sustainable living and leaves nature as untouched as possible. It is important to me that the tools I use, methods for delivery, and any aspect of my design and life is about being in harmony with my surroundings. I find the most beautiful things in life are natural. I strongly encourage making a contribution to helping the environment: adventuring a hike, thinking about your every day life process and what personal changes might help, or donating time and money to an environmental cause. If you need help with your environmental cause, especially web development help, please send me an email! It's on my resume.
