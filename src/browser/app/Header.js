// @flow
import type { State } from '../../common/types';
import React from 'react';
import linksMessages from '../../common/app/linksMessages';
import { Box } from '../../common/components';
import { FormattedMessage } from 'react-intl';
import { Link } from '../components';
import { compose } from 'ramda';
import { connect } from 'react-redux';

const HeaderLink = ({ to, message, ...props }) => (
  <FormattedMessage {...message}>
    {message => (
      <Link
        backgroundColor="primary"
        bold
        color="white"
        paddingHorizontal={0.5}
        paddingVertical={0.5}
        to={to}
        {...props}
      >
        {message}
      </Link>
    )}
  </FormattedMessage>
);

const Header = () => (
  <Box
    backgroundColor="primary"
    flexWrap="wrap"
    flexDirection="row"
    marginVertical={0.5}
    paddingHorizontal={0.5}
    style={() => ({ display: 'none' })}
  >
    <HeaderLink exact to="/" message={linksMessages.about} />
    <HeaderLink exact to="/" message={linksMessages.projects} />
    <HeaderLink exact to="/" message={linksMessages.thoughts} />
  </Box>
);

export default compose(
  connect((state: State) => ({ viewer: state.users.viewer })),
)(Header);
