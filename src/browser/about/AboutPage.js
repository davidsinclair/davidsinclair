// @flow
import React from 'react';
import {
  Box,
} from '../../common/components';
import { Title } from '../components';
import content from '../../common/content';

const AboutPage = () => (
  <Box>
    <Title message={content.pages.about.title} />
    <div
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{ __html: content.pages.about.content }}
    />
  </Box>
);

export default AboutPage;
