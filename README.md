# davidsinclair.io
Personal website of David Sinclair

## commands
- watch: `yarn start`
- build: `yarn start -- to-html`
- deploy: `yarn deploy`
- sync: `yarn sync`

## credits
made from [este](https://github.com/este/este)—thanks for providing such a great resource
